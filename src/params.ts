export class Params {
    cwd: string;
    source: string;
    destination: string;
    override?: boolean;
    include?: RegExp;
    exclude?: RegExp;
    flat?: boolean;
    header?: boolean;
}

export const fileTypeCommentMarker = {
    html: '<!--CONTENT -->',
    xml: '<!--CONTENT -->',
    js: '//',
    ts: '//',
    css: '//',
    scss: '//',
    less: '//',
    java: '//',
    kt: '//',
    c: '//',
    cpp: '//',
    cs: '//'
};
