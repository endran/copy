import {fileTypeCommentMarker, Params} from './params';
import * as fs from 'fs';
import * as rimraf from 'rimraf';
import {asyncForEach} from './util';

export class Copy {
    public async execute(params: Params): Promise<boolean> {
        try {
            this.assertSource(params.source);
            this.assertDestination(params.destination, params.override);

            await this.copyDir(params);
        } catch (e) {
            console.log(e);
            return false;
        }
        return true;
    }

    private assertSource(source: string) {
        if (!fs.lstatSync(source).isDirectory()) {
            throw Error(`${source} must be a directory.`);
        }
    }

    private assertDestination(destination: string, override: boolean) {
        if (fs.existsSync(destination)) {
            if (fs.lstatSync(destination).isFile()) {
                throw Error(`${destination} already exists and is a file.`);
            }
            if (fs.readdirSync(destination).length > 0 && !override) {
                throw Error(`${destination} already exists, must set --override to force execution.`);
            } else {
                rimraf.sync(destination);
            }
        }

        let dest = '';
        destination.split('/').forEach(dir => {
            dest = dest + dir;
            try {
                fs.mkdirSync(dest);
            } catch (e) {}
            dest = dest + '/';
        });
    }

    private async copyDir(params: Params, relativeSource: string = '') {
        const currentSourceDir = `${params.cwd}/${params.source}${relativeSource}`;

        await asyncForEach(fs.readdirSync(currentSourceDir), async path => {
            const currentPath = `${currentSourceDir}/${path}`;
            const currentRelativeSource = `${relativeSource}/${path}`;
            const currentDestination = `${params.cwd}/${params.destination}${currentRelativeSource}`;

            if (fs.lstatSync(currentPath).isFile() && params.include.test(currentPath) && (!params.exclude || !params.exclude.test(path))) {
                await this.copyFile(currentPath, currentDestination, params.header);
            } else if (fs.lstatSync(currentPath).isDirectory() && !params.flat) {
                fs.mkdirSync(currentDestination);
                await this.copyDir(params, currentRelativeSource);
            }
        });
    }

    private copyFile(source: string, destination: string, addHeader: boolean): Promise<any> {
        const handle: {
            resolve?: any;
            reject?: any;
        } = {};

        const res = new Promise((resolve, reject) => {
            handle.resolve = resolve;
            handle.reject = reject;
        });

        fs.writeFileSync(destination, '');
        const sourceStream = fs.createReadStream(source);
        const destinationStream = fs.createWriteStream(destination);

        const dotSplitted = destination.split('.');
        let fileType = dotSplitted[dotSplitted.length - 1];
        let commentMarker = (fileTypeCommentMarker[fileType] || '').split('CONTENT');

        if (addHeader && commentMarker[0]) {
            destinationStream.write(
                `${commentMarker[0]} ********************************************************************************************${
                    commentMarker[1] ? commentMarker[1] : ''
                }\n`
            );
            destinationStream.write(
                `${commentMarker[0]} *****   This is a generated file, do not edit. It is copied from:${
                    commentMarker[1] ? commentMarker[1] : ''
                }\n`
            );
            destinationStream.write(`${commentMarker[0]} *****   ${source}${commentMarker[1] ? commentMarker[1] : ''}\n`);
            destinationStream.write(
                `${commentMarker[0]} ********************************************************************************************${
                    commentMarker[1] ? commentMarker[1] : ''
                }\n`
            );
            destinationStream.write(`\n`);
        }

        sourceStream.pipe(destinationStream).on('close', handle.resolve);

        return res;
    }
}
