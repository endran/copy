export async function asyncForEach<T>(array: T[], callback: (t: T, index: number, array: T[]) => any): Promise<void> {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}