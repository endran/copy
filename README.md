# Copy

[![pipeline status](https://gitlab.com/endran/copy/badges/develop/pipeline.svg)](https://gitlab.com/endran/testdate/commits/master)
[![licence](https://img.shields.io/npm/l/@endran/copy.svg?style=flat)](https://opensource.org/licenses/MIT)
[![npm version](https://img.shields.io/npm/v/@endran/copy.svg?style=flat)](https://badge.fury.io/js/%40endran%2Fcopy)

Copy data from source to destination directories, with some fine grain control.

## Description

Sometime you need to copy data within your repo for your development process, for example when you are working with in a monorepo project. For this usecases `copy` is very well suited.

`copy` can copy data in an instant, and it can - if you so desire - add a warning header, so you can clearly see that the copies are generated code. Also using the power of regex, you can specify in detail which files need to be included, and which files needed to be excluded.  

## Installation

```bash
npm install @endran/copy --save
```

or

```bash
yarn add @endran/copy
```

## Usage

Basic usage, copy from source to destination:

```bash
yarn copy --source example/in --destination example/out
```

Power usage:
```bash
yarn copy -S example/in -D example/out -O -H // -I "txt$" -E .spec.
```

This will clear the destination folder, and then it will copy all files ending with `txt`, except for files that contain `.spec.`. It will add a comment header to each copied file, you need to specify the comment marker, so `copy` supports multiple file types.

## Examples

See the [examples](https://gitlab.com/endran/copy/tree/master/example) for all example input and output pairs.

`example/in/first.txt`:
```text
just
some
content
```

`example/out/first.txt`:
```text
// ********************************************************************************************
// ********************************************************************************************
// *****
// *****   This is a generated file, do not edit.
// *****   It is copied from /Users/endran/copy/example/in/first.txt.
// *****
// ********************************************************************************************
// ********************************************************************************************

just
some
content
```

## Help

```sh
Options:
  -V, --version             output the version number
  -S, --source <path>       Required. Location the source dir.
  -D, --destination <path>  Required. Location of output. Must be empty or --override must be true.
  -O, --override            Optional. If set will overwrite the destination dir.
  -I, --include <regex>     Optional. Regex for path inclusion. (default: ".*")
  -E, --exclude <regex>     Optional. Regex for path exclusion, operates on the included set.
  -F, --flat                Optional. Do not copy nested directories.
  -H, --header [value]      Optional. Add generated message file header, can be provided with language specific comment marker.
  -h, --help                output usage information
```

## License

```text
The MIT License

Copyright (c) 2019 David Hardy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
