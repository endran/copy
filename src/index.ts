#!/usr/bin/env node

import * as commander from 'commander';
import {Params} from './params';
import {Copy} from './copy';

const main = async function() {
    const cmd = commander
        .version('0.1.0')
        .option('-S, --source <path>', 'Required. Location the source dir.')
        .option('-D, --destination <path>', 'Required. Location of output. Must be empty or --override must be true.')
        .option('-O, --override', 'Optional. If set will overwrite the destination dir.')
        .option('-I, --include <regex>', 'Optional. Regex for path inclusion.', '.*')
        .option('-E, --exclude <regex>', 'Optional. Regex for path exclusion, operates on the included set.')
        .option('-F, --flat', 'Optional. Do not copy nested directories.')
        .option('-H, --header', 'Optional. Add generated message file header.')
        .parse(process.argv);

    const params: Params = {
        cwd: process.cwd(),
        source: cmd.source,
        destination: cmd.destination,
        override: cmd.override,
        include: new RegExp(cmd.include),
        exclude: cmd.exclude ? new RegExp(cmd.exclude) : undefined,
        flat: cmd.flat,
        header: cmd.header
    };

    if (params.source === undefined) {
        throw Error('--source is required');
    }
    if (params.destination === undefined) {
        throw Error('--destination is required');
    }

    const success = await new Copy().execute(params);

    console.log(success ? 'Complete successfully' : 'An error occurred');
    if (!success) {
        process.exit(1);
    }
};

if (require.main === module) {
    main()
        .then(() => {
            console.log(`Finished`);
            process.exit(0);
        })
        .catch(e => {
            console.log(`Finished with error`, e);
            process.exit(e.code || 1);
        });
}
